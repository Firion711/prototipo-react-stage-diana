var express = require('express');
var exphbs  = require('express-handlebars');

// module for request
const request = require('request');
// Setting Expresss
var app = express();
const port = 5000;

// Setting express-handlebars
app.engine('handlebars', exphbs({defaultLayout: 'main'}));

app.set('view engine', 'handlebars');

// indexing of pages with SSR
app.get('/ssr', (req, res) => {
    request('http://127.0.0.1:3000/:590b8481c8312c00077fd80f?ssr',(error, response, body) => {
        if (!error && response.statusCode == 200) {
            const data = JSON.parse(body);
            res.render('home',{ 
                html: data.html, 
                data: data.data,
                stylesheet: data.css
            })}
        else {
            console.log(error)
        }    
    })
});

// indexing of pages without SSR
app.get('*', function (req, res) {
    res.render('home');
});

app.listen(port,() => { console.log("Server is listening on port "+ port); });
