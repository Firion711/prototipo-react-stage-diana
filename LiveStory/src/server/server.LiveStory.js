import express from "express";
import React from'react';
import ReactDOM from'react-dom';
import { renderToString } from "react-dom/server";
import serialize from "serialize-javascript";
import 'isomorphic-fetch';
// Component import
import Generics from '../client/generics/generics';
// Fetch function
import fetchAPI from '../api/fetchAPI';
// My function
import {getStylesheetList} from '../utils/component-class-setting';

const app = express();

// add this middleware to Express to return .js.gz so you can still load bundle.js from the html but will receive bundle.js.gz
app.get('/bundle.js', function (req, res, next) {
  req.url = req.url + '.gz';
  res.set('Content-Encoding', 'gzip');
  next();
});

// Serving static files in Express in public folder
app.use(express.static("./public"));

// it's just a common markup template for testing one single template
function commonMarkupTemplateTest(IdComponent,html,dataSerialized) {
  return `<!DOCTYPE html>
          <html>
            <head><title>Live Story - test page</title></head>
            <script src="/bundle.js"></script>
          <body>
            <div id="LS-${IdComponent}">${html ? html: ""}</div>
            <script defer> window.renderComponent("${IdComponent}",${dataSerialized}); </script>  
          </body>
          </html>`;
}

// Endpoint made for testing the various Components
app.get('/::id/test', (req, res, next) => {
  console.log("Test zone"); 
  // Check if SSR is request
  const isSSR = req.query.ssr != undefined; 
  // Retrive the Component's name
  const IdComponent = req.params.id;
  console.log(IdComponent);
  let html = '';
  let dataSerialized;
  if (isSSR) { //SSR
    console.log("entro query SSR"); 
    fetchAPI(IdComponent)
    .then(data => data.json())
    .then(dataF => { 
          dataSerialized = serialize(dataF);
          html = renderToString(<Generics wall_id= {IdComponent} dataFetched= {dataF}/>);
      })
    .then(()=>res.send(commonMarkupTemplateTest(IdComponent,html,dataSerialized)));
  } // no SSR
  else {
    res.send(commonMarkupTemplateTest(IdComponent));
  }
});

// Endpoint that allow the SSR render of the components
app.get('/::id', (req, res, next) => {
  const IdComponent = req.params.id;
  console.log("Request from Brand: " + IdComponent);
  // Check if SSR is request
  const isSSR = req.query.ssr != undefined;
  console.log("isSSR: ",isSSR)
  if (isSSR) { // SE SSR
    console.log("son qui SSR")
    fetchAPI(IdComponent)
    .then(data => data.json())
    .then(dataF => {     
      const dataSerialized = serialize(dataF);
      //Get the stylesheet links
      let links = getStylesheetList([dataF.theme, dataF.style]);
      console.log('SERVER',links); 
 
      const html = renderToString(<Generics  wall_id = {IdComponent} dataFetched = {dataF}/>);  
      res.send({ html: html, data: dataSerialized, css: links});
    })
  .catch(err => console.log(err) )}
    else { // with no SSR request, the server do nothing, just serve the bundle file
      console.log("son qui NO SSR")
      return null;
    } 
  });

  app.listen(process.env.PORT || 3000, () => {
    const port = process.env.PORT || 3000;
    console.log("Server Live Story is listening on port: " + port);
  });
  