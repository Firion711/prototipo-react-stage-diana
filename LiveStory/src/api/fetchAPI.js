import { log } from "util";

// il fetch è eseguito attraverso un url fisso + un indirizzo id che verrà passato alla funzione
const url = 'https://frontendapi-dev.livestory.io:5002/front/v4/wall/';

function fetchAPI(id) {
    if(id) {
        console.log("fetchando...")
        return fetch(url + id);    
    }
    else {
        console.log("id mancante...")
        return null;
    }
}

export default fetchAPI;