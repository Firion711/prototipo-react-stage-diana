import $ from "jquery";

export function componentClassNameSetting(theme, color_scheme) {
    return "LiveStory-component " + theme + ' ' + color_scheme; 
}

export function noOverflowDisplayPopup(state_bool) {
    console.log("Display popup: ", state_bool);  
    if(state_bool)
        $('body').css('overflow', 'hidden');
    else
        $('body').css('overflow', 'initial');
}

// funzione di supporto che 
export function displaySvg(className) {
    console.log("dispaySvg on: ", className);
    $('.' + className).removeClass('LS-noSvg');
}
/***************************************************************
 *  Stylesheet setting
 ****************************************************************/

// carica gli stylesheet nel document se non sono già presenti
export function loadStylesheetOnDocument(links) {   
    links.forEach( element => {
        if (!document.getElementById('LS-' + element)) {
            let link=document.createElement('link');
            link.href=createStylesheetPath(element);
            link.id= createStylesheetId(element);
            link.rel='stylesheet';
            document.head.appendChild(link);
        }
    })    
}

// ritorna un id da assegnare allo stylesheet
function createStylesheetId(name) {
    return 'LS-' + name;
}

// genera la stringa dello stylesheet
function createStylesheetString(name,path) {
    const link_id = createStylesheetId(name);
    return '<link id="'+ link_id + '" rel="stylesheet" href="'+ path +'">';
}

// crea il percorso href per gli stylesheet
function createStylesheetPath(name) {
    const COMMON_PATH = "http://127.0.0.1:3000/css/";    
    let link = COMMON_PATH + name + '.css';
    return link;
}

// nel caso ci siano dei nomi doppi, li rende unici
function distinct(array) {
    return Array.from(new Set(array));
}

export function getStylesheetList(links){
    const COMMON_PATH = "http://127.0.0.1:3000/css/";
    distinct(links);
    let result_link = [];
    links.forEach(element => {
        result_link.push(createStylesheetString(element,createStylesheetPath(element)))
    });
    return result_link;
}