const commonPath = "./components";

const componentsConfig = {
  "grid":  {
      "wall-grid-default": {
        name: "Grid",
        path: commonPath + '/grid/grid'
      },
      "wall-grid-calzedonia-1": { //esempio
        name: "GridCustom",
        path: commonPath + '/grid/grid-custom'
      }
  },

  "strip": {
    "wall-strip-default": {
      name: "Strip",
      path: commonPath + '/strip/strip'
    },
  },
};

export default componentsConfig;

