import React from 'react';
import ReactDOM from'react-dom';

// Component import
import Generics from './generics/generics';

window.renderComponent = function(id, data) {
    console.log("renderComponent",id);  
    ReactDOM.render(<Generics wall_id={id} dataFetched= {data}/>, document.getElementById('LS-'+id));
}
