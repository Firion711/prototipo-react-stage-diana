import React from 'react';
import {componentClassNameSetting} from '../../../utils/component-class-setting';
import $ from 'jquery';
import Wall from '../wall/wall';
 
class Strip extends Wall {

  scrollTo(direction) {
    var box = $(".wrapper-img"), x;
    if (direction == "left") 
      x = -((box.width() / 2)) + box.scrollLeft();
    else 
     x = ((box.width() / 2)) + box.scrollLeft();
     
    box.animate({
      scrollLeft: x,
    })
  }  

  setLayout(option) {
    const height = parseInt(option.height) + parseInt(option.margin);
    const margin = parseInt(option.margin)/2;
    const theme = '.' + this.props.dataFetched.theme;
    // main container component
    $(theme).css('height', height).css('max-width', option.maxwidth);
    // wrapper-img
    $(theme + ' .wrapper-img').css('height',height);
    // set thumbnail img
    $(theme + ' .thumbnail').css('height', option.height).css('margin', margin);
  }

  render() {
    return (
      <div className={componentClassNameSetting(this.props.dataFetched.theme, this.props.dataFetched.style)}> 
        {/* left arrow */}
        <a className="arrow-left arrow" onClick={this.scrollTo.bind(this,"left")}><svg viewBox="0 0 2048 2048" xmlns="http://www.w3.org/2000/svg"><path d="M1331 672q0 13-10 23l-393 393 393 393q10 10 10 23t-10 23l-50 50q-10 10-23 10t-23-10l-466-466q-10-10-10-23t10-23l466-466q10-10 23-10t23 10l50 50q10 10 10 23z"/></svg></a>
        {/* right arrow */}
        <a className="arrow-right arrow" onClick={this.scrollTo.bind(this,"right")}><svg viewBox="0 0 2048 2048" xmlns="http://www.w3.org/2000/svg"><path d="M1299 1088q0 13-10 23l-466 466q-10 10-23 10t-23-10l-50-50q-10-10-10-23t10-23l393-393-393-393q-10-10-10-23t10-23l50-50q10-10 23-10t23 10l466 466q10 10 10 23z"/></svg></a>
        {/* content */}
        <div className="wrapper-img">
          {this.commonRender()}
        </div>
    </div>)
  }
}

export default Strip;
