import React, {Component} from 'react';

class PopupAsset extends Component {   
    constructor(props) {
        super(props);

        this.state = {
            currentAsset: {
                data: this.props.extractInfo(this.props.dataAsset[0]),
                index: 0
            }}
    }
    
    isFirstChild() {
        return this.state.currentAsset.index == 0;
    }

    isLastChild() {
        return this.state.currentAsset.index == (this.props.assetLenght -1);
    }

    incrementAssetIndex() {
        const new_index = this.state.currentAsset.index + 1;
        this.setCurrentAsset(new_index);
    }

    decrementAssetIndex() {
        const new_index = this.state.currentAsset.index - 1;
        this.setCurrentAsset(new_index);
    }

    setCurrentAsset(new_index) {
        this.setState({
            currentAsset: {
                data: this.props.extractInfo(this.props.dataAsset[new_index]),
                index: new_index
            }})
    }

    render() {
        return ( 
            <div>
                <div className="popup-asset"> 
                {/* Freccia di sinistra che non compare sul primo elemento */}
                { this.isFirstChild() ? null : <a className="arrow-asset arrow-asset-left" onClick={this.decrementAssetIndex.bind(this)}><svg viewBox="0 0 2048 2048" xmlns="http://www.w3.org/2000/svg"><path d="M1331 672q0 13-10 23l-393 393 393 393q10 10 10 23t-10 23l-50 50q-10 10-23 10t-23-10l-466-466q-10-10-10-23t10-23l466-466q10-10 23-10t23 10l50 50q10 10 10 23z"/></svg></a>}
                            <div className="popup-asset-content">
                                <div className="popup-asset-thumbnail"><img src={this.state.currentAsset.data.pictures[0].original_url}/></div>
                                <div className="popup-asset-title">{this.state.currentAsset.data.name}</div>
                                <div className="popup-asset-price">{this.state.currentAsset.data.price}</div>            
                            </div>
                {/* Freccia di destra che non compare sull'ultimo elemento */}
                { this.isLastChild() ? null :<a className="arrow-asset arrow-asset-right" onClick={this.incrementAssetIndex.bind(this)}><svg viewBox="0 0 2048 2048" xmlns="http://www.w3.org/2000/svg"><path d="M1299 1088q0 13-10 23l-466 466q-10 10-23 10t-23-10l-50-50q-10-10-10-23t10-23l393-393-393-393q-10-10-10-23t10-23l50-50q10-10 23-10t23 10l466 466q10 10 10 23z"/></svg></a>}
                    
                </div>
                <div className="popup-asset-button"><div className="asset-button"> Shop now </div></div>   
            </div>
         );
    }
}

export default PopupAsset;