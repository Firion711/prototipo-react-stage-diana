import React, {Component} from 'react';
import {componentClassNameSetting, noOverflowDisplayPopup,loadStylesheetOnDocument,displaySvg} from '../../../utils/component-class-setting';
import OverlayFade from '../overlay-fade/overlay-fade';
import PopupDisplayContent from '../popup-display-content/popup-display-content';

class Wall extends Component {
    constructor(props) {
        super(props)
        this.state = {
            display_popup: false,
            currentIndex: 0
        }
    // bind functions
    this.handleClickDisplayPopup = this.handleClickDisplayPopup.bind(this);
    this.incrementIndex = this.incrementIndex.bind(this);
    this.decrementIndex = this.decrementIndex.bind(this);
    }
    // i componenti che ereditano da questa classe avranno lo stile e settato secondo la lora implementazione di setLayout e caricheranno i loro specifici stylesheet  
    componentDidMount(){
        const theme = this.props.theme;
        const color_scheme = this.props.color_scheme;
        if ( theme && color_scheme) {
            loadStylesheetOnDocument([theme,color_scheme]);
            displaySvg(theme);
        }
        this.setLayout(this.props.layout);
    }
    
    //gestisce l'evento click per rendere visibile il component PopupDisplayContent con i relativi dati, selezionati dal relativo indice 
    handleClickDisplayPopup(state_bool,index) { 
        noOverflowDisplayPopup(state_bool);   
        this.setState({
            display_popup: state_bool,
            currentIndex: index })
    }
    // controllo booleano per verificare se lo stato che salva l'indice(currentIndex) corrisponde    
    currentIndex(index) {
        return index == this.state.currentIndex;
    }
    
    // quando di visualizza PopupDisplayContent, al click della freccia di destra, se presente, aumenta l'indice per visualizzare l'evento sucessivo
    incrementIndex(index) {
        this.setState({
            currentIndex: index + 1})
    }
    
    // quando di visualizza PopupDisplayContent, al click della freccia di sinistra, se presente, diminuisce l'indice per visualizzare l'evento precedente
    decrementIndex(index) {
        this.setState({
            currentIndex: index - 1})
    }

    renderPopup(object) {
        return <PopupDisplayContent 
                    key={"popup-display-content-"+ object.index} 
                    displayPopup={this.handleClickDisplayPopup} 
                    dataDetail={object.element}
                    dataProduct={this.props.dataFetched.products} 
                    index={object.index} 
                    incrementIndex={this.incrementIndex}
                    decrementIndex={this.decrementIndex}
                    elementsLenght={object.lenghtOfPosts}/>
    }

    renderOverlay(object) {
       return <OverlayFade
                key={"overlay-fade-"+ object.index} 
                text={"view"} 
                displayPopup={this.handleClickDisplayPopup} 
                index={object.index}>

                    <img className="thumbnail" alt={object.element.hashtags[0]} key={object.element._id} src={object.element.pictures[0].high.url}/> 
            </OverlayFade>
    }

    // Parte di rendering comune per i componenti "figli", creata quindi per modularità generica e comune. 
    // Lo scopo principale è quello di visualizzare l'overlay sull'immagine e il popup di dettaglio di essa per ogni elemento presente nell'oggetto posts           
    commonRender() {
        const lenghtOfPosts = Object.keys(this.props.dataFetched.posts).length;
        return (this.props.dataFetched.posts.map((element,index) => (
            <div> 
                {this.state.display_popup && this.currentIndex(index) && this.renderPopup({element,index,lenghtOfPosts})}
                {this.renderOverlay({element,index})}
            </div>)));
        }  

    render() {
        return "Sono in Wall component"
    }    
}
    
export default Wall;