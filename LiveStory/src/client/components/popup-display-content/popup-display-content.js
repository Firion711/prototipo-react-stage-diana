import React, {Component} from 'react';
import PopupAsset from '../popup-asset/popup-asset';

class PopupDisplayContent extends Component {
    constructor(props){
        super(props);
        // bind function
        this.extractInfo = this.extractInfo.bind(this);
    }

    /***********************************************************************
     * Logical functions
     ************************************************************************/
    isFirstChild(){
        return this.props.index == 0;
    }

    isLastChild() {
        return this.props.index == (this.props.elementsLenght - 1);
    }

    incrementIndex(index) {
        this.props.incrementIndex(index);
    }

    decrementIndex(index) {
        this.props.decrementIndex(index);
    }

    handleClickClosePopup(){
        this.props.displayPopup(false);
    }
    
    extractInfo(id) {
        return this.props.dataProduct.find(element => element._id == id);
    }

    /***********************************************************************
     * Render functions
     ************************************************************************/
    renderLeftArrow() {
        return (this.isFirstChild() ? null : <a className="arrow-left arrow" onClick={this.decrementIndex.bind(this,this.props.index)}><svg viewBox="0 0 2048 2048" xmlns="http://www.w3.org/2000/svg"><path d="M1331 672q0 13-10 23l-393 393 393 393q10 10 10 23t-10 23l-50 50q-10 10-23 10t-23-10l-466-466q-10-10-10-23t10-23l466-466q10-10 23-10t23 10l50 50q10 10 10 23z"/></svg></a>);
    }

    renderRightArrow() {
        return (this.isLastChild() ? null :<a className="arrow-right arrow" onClick={this.incrementIndex.bind(this,this.props.index)}><svg viewBox="0 0 2048 2048" xmlns="http://www.w3.org/2000/svg"><path d="M1299 1088q0 13-10 23l-466 466q-10 10-23 10t-23-10l-50-50q-10-10-10-23t10-23l393-393-393-393q-10-10-10-23t10-23l50-50q10-10 23-10t23 10l466 466q10 10 10 23z"/></svg></a>);
    }

    renderClosePopup() {
        return <div className="popup-close" onClick={this.handleClickClosePopup.bind(this)}>X</div>;
    }

    renderUsernameTitle() {
        return <div className="popup-username">@{this.props.dataDetail.author.username}</div>;
    }

    renderThumbnail() {
        return <div className="popup-thumbnail"><img src={this.props.dataDetail.pictures[0].high.url}/></div>;
    }

    renderDescription() {
        return <div className="popup-description">{this.props.dataDetail.text}</div>
    }

    renderPopupAsset() {
        return Object.keys(this.props.dataDetail.assets).length ? <PopupAsset dataAsset={this.props.dataDetail.assets} key={this.props.index} assetLenght = {Object.keys(this.props.dataDetail.assets).length} extractInfo={this.extractInfo}/> : null;
    }

    renderPopupCol1() {
        return <div className="popup-col-1">{this.renderThumbnail()}</div>;
    }

    renderPopupCol2() {
        return <div className="popup-col-2">
                    {this.renderDescription()}
                    {this.renderPopupAsset()}
                </div>;
    }

    renderContent() {
        return (<div className="popup-content">
            {/* x for close popup */}
            {this.renderClosePopup()}
            {/* username */}
            {this.renderUsernameTitle()}
            {/* image  */}
            {this.renderPopupCol1()}
            {this.renderPopupCol2()}          
            </div>)
    }

    render() {
        let data_asset = this.props.dataDetail.assets;
        return (
            <div className="popup-main">
                <div className="popup-background">
                    <div className="popup-wrapper">
                        {/* Left arrow, hidden on first element */}
                        {this.renderLeftArrow()}
                        {/* Render the content of popup */}
                        {this.renderContent()}
                        {/* Right arrow, hidden on last element */}
                        {this.renderRightArrow()}
                    </div>
                </div>
            </div>)}
    }
    
export default PopupDisplayContent;