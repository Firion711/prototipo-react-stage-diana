import React from 'react';
import PopupDisplayContent from './popup-display-content';

class PopupDisplayContentCalzedonia1 extends PopupDisplayContent {
    handleClick() {
        window.open(this.props.dataDetail.url, '_blank');;
    }
    renderPopupCol2() {
        return <div className="popup-col-2">
                    {this.renderDescription()}
                    {this.renderPopupAsset()}
                    <div className="popup-calzedonia-1-button"><div className="asset-button" onClick={this.handleClick.bind(this)}> Open in a new tab </div></div>
                </div>;
    }
}

export default PopupDisplayContentCalzedonia1;