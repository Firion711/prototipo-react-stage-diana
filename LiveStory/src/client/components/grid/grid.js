import React from 'react';
import {componentClassNameSetting} from '../../../utils/component-class-setting';
import Wall from '../wall/wall';
import $ from "jquery";

class Grid extends Wall {
  setLayout(option) {
    console.log("this.setLayout", option);   
    const theme = '.' + this.props.theme;
    // main container component
    $(theme).css('max-width', option.maxwidth);  
    if (option.square){
      const columns = parseInt(option.columns);
      const margin = parseInt(option.margin)/2;
      // set thumbnail img  
      const item_width =  Math.floor((($('.container-flex').width() - 14 - (columns * 2 * margin) )/columns ));  
      $(theme + ' .container_overlay_fade').css('width', item_width+'px').css('height', item_width+'px').css('margin', margin);
    }   
  }
  
  render() {
    return ( 
      <div className={componentClassNameSetting(this.props.dataFetched.theme, this.props.dataFetched.style)}>    
        <div className="title-grid">{this.props.dataFetched.title}</div>
        <div className="container-flex"> 
        {this.commonRender()}
      </div>
      {/* <div className="loadMore"><a className="button">Load More</a></div> */}
      </div>)
  }
}
    
export default Grid;
    
// Link da utilizzare per "vedere" i path delle foto
// https://mediacdn.livestory.io/v1
// https://livestory-media.s3-eu-west-1.amazonaws.com/