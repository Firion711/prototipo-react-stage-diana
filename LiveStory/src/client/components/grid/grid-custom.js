import React from 'react';
import Grid from './grid';
import PopupDisplayContentCalzedonia1 from '../popup-display-content/popup-display-content-calzedonia-1';

class GridCustom extends Grid {
    renderPopup(object) {
        return <PopupDisplayContentCalzedonia1 
                    key={"popup-display-content-"+ object.index} 
                    displayPopup={this.handleClickDisplayPopup} 
                    dataDetail={object.element}
                    dataProduct={this.props.dataFetched.products} 
                    index={object.index} 
                    incrementIndex={this.incrementIndex}
                    decrementIndex={this.decrementIndex}
                    elementsLenght={object.lenghtOfPosts}/>
    }
}

export default GridCustom;