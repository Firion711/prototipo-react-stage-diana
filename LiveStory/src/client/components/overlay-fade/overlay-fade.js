import React, { Component } from 'react';

class OverlayFade extends Component {
    
    handleClickDisplayPopup(state_bool,index) {     
        this.props.displayPopup(state_bool,index);
    }
    
    render() {
        return(
            <div className="container_overlay_fade">
                {this.props.children}
                <div className="overlay"><a className="overlay-button" onClick={this.handleClickDisplayPopup.bind(this,true,this.props.index)}>{this.props.text}</a></div>
            </div>)
    }
}

export default OverlayFade;