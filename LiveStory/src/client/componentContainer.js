import componentsConfig from './componentsConfig';

let components = {};
// Funzione che esegue la require solo dei moduli necessari
export default function dynamicImportOfComponent(subtype, theme) {
  if(subtype && theme) {
    const component = componentsConfig[subtype];
    const component_name = component[theme];
    if (component_name) {
      console.log("Effettuo il require");
      components[component_name.name] = require(`${component_name.path}`).default;
      return components[component_name.name];
    }
  }
}
