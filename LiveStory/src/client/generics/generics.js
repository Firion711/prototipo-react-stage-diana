import React from 'react';

// Component import
import ComponentContainer from '../componentContainer';
import componentsConfig from '../componentsConfig';
import dynamicImportOfComponent from '../componentContainer';
import Spinner from '../components/spinner/spinner';
import {loadStylesheetOnDocument} from '../../utils/component-class-setting';
// jQuery
import $ from "jquery";

// Fetch module
import fetchAPI from '../../api/fetchAPI';

export default class Generics extends React.Component {
    constructor(props){
        super(props);
        
        let initialData;
        let loadState;
        let isSSR;

        // caso1: i dati son già stati fetchati dal server
        if (this.props.dataFetched) {
            console.log("caso 1: SSR")
            initialData = this.props.dataFetched;           
            loadState = false;
            isSSR = true;
        }
        //caso2: siamo nel browser ma il component non ha subito un pre-render -> fetcha adesso i dati
        else {
            console.log("caso2: no SSR")
            initialData = null;
            loadState = true;
            isSSR = false;
        }
        //salvo i valori nello state del 
        this.state = { 
            dataFetched: initialData,
            isLoading: loadState,
            isSSR: isSSR
        };
    }
    
    componentDidMount() {
        // Si carica lo spinner solo se è renderizzato client side
        if (!this.state.isSSR) {
            loadStylesheetOnDocument(['LS-spinner']);
        }
        // Se non ho fatto SSR fetcho dopo un primo render
        if (!this.props.dataFetched) {
            console.log("Generics: Component Did Mount");      
            // Di default faccio ritornare 20 risultati
            fetchAPI(this.props.wall_id)
            .then(res => res.json())
            .then(data => this.setState({ dataFetched: data}))
            .then(() => this.setState({isLoading: false}))
            .catch( err => console.log(err));
        }
    }
    
    // Set the color scheme on the component
    setColorScheme(data) {
        let style_name = data.style;
        console.log("color scheme:", style_name);
        
        //if the color scheme is not found, then select the default one
        if (!style_name) { 
            console.log("Error: theme not found");
            style_name = "wall-light"; // default theme
        }     
        return style_name;       
    }
    
    render() {     
        console.log("Rendering...");
        // variable set
        const dataFetched = this.state.dataFetched;
        const loading = this.state.isLoading;
        let value;

        // Spinner showed until the content is fully loaded
        value = <Spinner/>;
        
        if (!loading) {
            // set color scheme selected
            const color_scheme = this.setColorScheme(dataFetched);
            
            // set theme
            const theme = dataFetched.theme;
            console.log("theme:",theme);
            // Load component
            const Component = dynamicImportOfComponent(dataFetched.subtype,theme); 
            
            // Extract layout css
            const layout = dataFetched.layout;

            const links = {
                theme: "../css/"+theme+".css",
                color_scheme: "../css/"+color_scheme+".css",
            };
                    
            // Render Component         
            value = <Component
                        className={'LS-' + this.props.wall_id} 
                        key={this.props.wall_id} 
                        dataFetched={dataFetched}
                        layout={layout}
                        color_scheme={color_scheme}
                        theme={theme} />
        }
            
    return value;
    }
}

