const gulp = require('gulp');
const sass = require('gulp-sass'); //used fo compiled sass
const autoprefixer = require('gulp-autoprefixer'); // add vendor
const cssMin = require('gulp-css'); // minify css
const rename = require('gulp-rename'); //used for changed the folder path and rename files
const webpack = require('webpack-stream');

const webpackConfigClient = require('./webpack.config/client/webpack.config.js');
const webpackConfigServer = require('./webpack.config/server/webpack.config.js');

// compile sass into css
gulp.task('sass', () => {
    return gulp.src(['./src/style/components/*.scss', './src/style/color_scheme/**/*.scss'])
    .pipe(sass())
    .pipe(autoprefixer())
    .pipe(cssMin())
    .pipe(rename({dirname: ''}))
    .pipe(gulp.dest('./public/css/'))
})

// build the bundle
gulp.task('build_client', () => {
    return gulp.src('./src/client/index.js')
    .pipe(webpack(webpackConfigClient))
    .pipe(gulp.dest('public/')); 
});

// build the server file
gulp.task('build_server', () => {
    return gulp.src('./src/server/server.LiveStory.js')
    .pipe(webpack(webpackConfigServer))
    .pipe(gulp.dest('dist/'));
});

// used for production mode, build everuthing
gulp.task('build', ['build_client', 'build_server', 'sass'] );

// watch the changes on scss and re-compile
gulp.task('watch_css', () => {
    gulp.watch('./src/style/**/*.scss', ['sass'])
})

// watch the changes on src folders and re-compile the bundle
gulp.task('watch_client', () => {
    gulp.watch(['./src/**/*.js', '!./src/server/*.js'], ['build_client'])
}) 

// watch the changes on the server and re-compile it
gulp.task('watch_server', () => {
    gulp.watch('./src/server/*.js', ['build_server'])
}) 

// just merge all the watch tasks
gulp.task('watch_all', ['watch_css', 'watch_client','watch_server']);

// used in development mode, recompile on change
gulp.task('default', ['watch_all']);