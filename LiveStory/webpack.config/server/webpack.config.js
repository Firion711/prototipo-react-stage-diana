const webpack = require("webpack");
const WebpackBundleSizeAnalyzerPlugin = require('webpack-bundle-size-analyzer').WebpackBundleSizeAnalyzerPlugin;

const serverConfig = {
  entry: "./src/server/server.LiveStory.js",
  target: "node",
  output: {
    path: __dirname,
    filename: "./server.js"
  },
  module: {
    rules: [
      {
        test: /js$/,
        exclude: /(node_modules)/,
        loader: "babel-loader",
        query: { 
          presets: ['es2015','react', 'stage-2'] 
        }
      }
    ]
  },
  plugins: [
    new WebpackBundleSizeAnalyzerPlugin('../../reports/plain-report.server.txt'),
  ]
};

module.exports = serverConfig;
